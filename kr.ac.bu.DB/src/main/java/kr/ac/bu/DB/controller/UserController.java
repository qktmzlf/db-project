package kr.ac.bu.DB.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.bu.DB.domain.User;
import kr.ac.bu.DB.service.UserService;


@Controller
@RequestMapping("user")
public class UserController {

   @Autowired
   private UserService service;

   @RequestMapping(value = "login.do", method = RequestMethod.POST)
   public ModelAndView login(Model model, HttpSession session, String userId, String userPw) {
      

      User user = new User();

      user.setId(userId);
      user.setPw(userPw);

      user = service.login(user);

      
   ModelAndView modelAndView = new ModelAndView();

   if(user.getPw() == null && user.getId() == null)   {
       model.addAttribute("loginFail", "ID/PW를 확인하세요");
       modelAndView.setViewName("user/login");   
   }else if(user.getPw().equals(userPw) && user.getId().equals(userId)) {
      session.setAttribute("user", user);
      modelAndView.setViewName("calendar/cal");   
   }
   else   {
      model.addAttribute("loginFail", "ID/PW 다시입력해주세요");
   }
   return modelAndView;
}

      
   @RequestMapping(value = "delete.do", method = RequestMethod.POST)
   public String delete(Model model, HttpSession session, String userPw) {
      User user = new User();
      user = (User)session.getAttribute("user");
      String userId = user.getId();
      
      if(service.delete(userId, userPw)){
         session.setAttribute("user", null);
         return "user/login";
      }else {
         return "user/deleteconfirm";
      }
   }
   
   @RequestMapping(value = "modify.do")
   public String modify(Model model, HttpSession session) {
      User user = new User();
      user = (User)session.getAttribute("user");
      model.addAttribute("users", user);
      return "common/modify";
   }
   
   @RequestMapping(value = "modify.do", method = RequestMethod.POST)
   public String modify(Model model, HttpSession session, User user) {
      
      System.out.println("modify"+user.getId());
      if(service.modify(user))   {
         return "redirect:/user/information.do";
      }else
         return "redirect:/user/modify.do";
   }
   @RequestMapping(value = "logout.do")
   public String logout(HttpSession session) {
	   session.setAttribute("user", null);
	   return "user/login";
   }
}



