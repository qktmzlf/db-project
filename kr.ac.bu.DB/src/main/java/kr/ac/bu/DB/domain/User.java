package kr.ac.bu.DB.domain;


import java.sql.Date;
import java.util.List;

public class User {

	private String id;
	private String pw;
	private String name;
	private String phone;
	private Date birth;
	private String addr;
	private String memberId;
	private int point;
	private List<Reserve> reserve;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Date getBirth() {
		return birth;
	}
	public void setBirth(Date birth) {
		this.birth = birth;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public int getPoint() {
		return point;
	}
	public void setPoint(int point) {
		this.point = point;
	}
	public List<Reserve> getReserve() {
		return reserve;
	}
	public void setReserve(List<Reserve> reserve) {
		this.reserve = reserve;
	}
	
	
	
}