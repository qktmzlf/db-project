package kr.ac.bu.DB.service;

import kr.ac.bu.DB.domain.User;

public interface UserService {
	//
	User login(User user);
	boolean register(User user);
	boolean delete(String userId, String userPw);
	boolean modify(User user);
}
