package kr.ac.bu.DB.service.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.ac.bu.DB.domain.User;
import kr.ac.bu.DB.service.UserService;
import kr.ac.bu.DB.store.UserStore;

@Service
public class UserServiceLogic implements UserService {

   @Autowired
   private UserStore store;

   @Override
   public User login(User user) {

      user = store.login(user);

      return user;
   }

   @Override
   public boolean register(User user)  {

      System.out.println("service " + user.getId());
      
      store.create(user, store.create()); // memberID의 값을 스트링으로 두번째 파라미터에 넣어준다.
      

      return false;
   }


   @Override
   public boolean delete(String userId, String userPw) {
      return store.delete(userId, userPw);
   }

   @Override
   public boolean modify(User user) {
      return store.modify(user);
   }



}