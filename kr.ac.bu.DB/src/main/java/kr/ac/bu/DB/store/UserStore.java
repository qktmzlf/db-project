package kr.ac.bu.DB.store;

import java.util.List;

import kr.ac.bu.DB.domain.Reserve;
import kr.ac.bu.DB.domain.User;

public interface UserStore {
	//
	boolean create(User user, String meberId);
	String create();
	User login(User user);
	boolean delete(String userId, String userPw);
	boolean modify(User user);
	List<Reserve> reserve(String id);
		
}
