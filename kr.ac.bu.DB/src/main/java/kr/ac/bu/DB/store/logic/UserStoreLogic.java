package kr.ac.bu.DB.store.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;

import kr.ac.bu.DB.domain.Reserve;
import kr.ac.bu.DB.domain.User;
import kr.ac.bu.DB.store.UserStore;
import kr.ac.bu.DB.store.factory.ConnectionFactory;
import kr.ac.bu.DB.store.logic.util.JdbcUtils;

@Repository
public class UserStoreLogic implements UserStore {

	private ConnectionFactory connectionFactory;

	public UserStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Override
	public boolean create(User user, String memberId) {

		Connection connection = null;
		PreparedStatement psmt = null;

		int createdCount = 0;
		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement(
					"INSERT INTO usertbl(userId, userPw, userName, userPhone, userBirth, userAddr, memberId) VALUES (?, ?, ?, ?, ?, ?, ?)");

			psmt.setString(1, user.getId());
			psmt.setString(2, user.getPw());
			psmt.setString(3, user.getName());
			psmt.setString(4, user.getPhone());
			psmt.setDate(5, user.getBirth());
			psmt.setString(6, user.getAddr());
			psmt.setString(7, memberId);
			

			createdCount = psmt.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, connection);
		}

		return createdCount > 0;
	}

	@Override
	public String create() {

		String memberId = null;

		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;

		int createdCount = 0;
		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement("INSERT INTO membertbl(memberId, memberpoint) VALUES (null, 0)");

			createdCount = psmt.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, connection);
		}

		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement("select memberId from membertbl");

			rs = psmt.executeQuery();

			while (rs.next()) {
				memberId = rs.getString(1);
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, connection);
		}

		return memberId;
	}

	@Override
	public User login(User user) {

		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;

		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement(
					"SELECT userId, userPw, userName, userPhone, userBirth, userAddr, memberId from usertbl where userId = ?");

			psmt.setString(1, user.getId());

			rs = psmt.executeQuery();

			if (rs.next()) {
				user = new User();
				user.setId(rs.getString(1));
				user.setPw(rs.getString(2));
				user.setName(rs.getString(3));
				user.setPhone(rs.getString(4));
				user.setBirth(rs.getDate(5));
				user.setAddr(rs.getString(6));
				user.setMemberId(rs.getString(7));

			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, connection, rs);
		}

		return user;
	}

	@Override
	public boolean delete(String userId, String userPw) {

		Connection connection = null;
		PreparedStatement psmt = null;
		int delSuccess = 0;

		System.out.println("store" + userId + userPw);

		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement("delete from usertbl where userId = ? and userPw = ?");

			psmt.setString(1, userId);
			psmt.setString(2, userPw);

			delSuccess = psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			JdbcUtils.close(psmt, connection);
		}
		return delSuccess > 0;
	}

	@Override
	public boolean modify(User user) {
		Connection conn = null;
		PreparedStatement psmt = null;
		int updateCount = 0;
		try {
			conn = connectionFactory.createConnection();

			conn = connectionFactory.createConnection();
			String sql = "UPDATE usertbl SET userPw = ?, userName = ?, userPhone = ? WHERE userId = ?";
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, user.getPw());
			psmt.setString(2, user.getName());
			psmt.setString(3, user.getPhone());
			psmt.setDate(4, user.getBirth());
			psmt.setString(5, user.getAddr());
			psmt.setString(6, user.getId());
			updateCount = psmt.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
		return updateCount > 0;

	}

	@Override
	public List<Reserve> reserve(String id) {
		// TODO Auto-generated method stub
		return null;
	}

}