﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>



<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<title>Plan</title>
<style>
</style>

</head>
<body>

	<nav class="navbar navbar-light" style="background-color: #e3f2fd;">
		<a class="navbar-brand" href="#"
			onclick='history.back(-1); return false;'>뒤로가기</a>
	</nav>
	<div class="page-wrapper">
		<div class="container-fluid">
		<!-- 	<div class="">
				<div class="">  -->
					<h1 class="page-header text-center">영화 정보</h1>
				</div>
				<div class="row"></div>
				<!-- <div class="panel panel-default"> -->
					<!-- 	<div class="panel-heading"> -->
					<div class="container marketing">
						<span style="font-size: 18pt">${movie.name}</span>
						<!-- </div> -->
						 <div class="panel-body">
							<table class="table table-hover">
								<tr>
									<th class="bg-info">영화이름</th>
									<td>${movie.name}</td>
								</tr>
								<tr>
									<th class="bg-info">장르</th>
									<td>${movie.genre}</td>
								</tr>
								<tr>
									<th class="bg-info">연령제한</th>
									<td>${movie.age}</td>
								</tr>
								<tr>
									<th class="bg-info">감독</th>
									<td>${movie.dir}</td>
								</tr>
								<tr>
									<th class="bg-info">배우</th>
									<td>${movie.actor}</td>
								</tr>
							</table>
							<a
								href="${pageContext.request.contextPath}/board/modify.do?planId=${param.planId}"
								class="glyphicon glyphicon-cog pull-right" style="padding: 10px">수정</a>
							<a
								href="${pageContext.request.contextPath}/board/delete.do?planId=${param.planId}"
								class="glyphicon glyphicon-trash pull-right"
								style="padding: 10px">삭제</a>
						</div>
				 	
				<a href="${pageContext.request.contextPath}/board/boardList.do">목록으로</a>
			</div> 
			
			</div>


		<br>

</body>
</html>
