<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<meta charset="UTF-8">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/jquery-2.1.3.js"></script>
<script>
	$(function(){
		var movieId;
		$('#sDate').change(function(){
			/* movieId = this.value; */
			movieId = $(this).children(":selected").attr("id");
			alert(movieId);
			$.ajax({
				url : "${pageContext.request.contextPath}/movie/theaters.do",
				async: true, 
			    type: 'GET' ,
			    data: {
			    	movieId : movieId,
			    }, 
			    dataType: 'json',
			    success: function(result) {
			    	for (var i = 0; i < result.length; i++) {
			    		 $("#theaterArea").append("<option value='"+result[i].theaterArea+"'>"+result[i].theaterArea+"</option>");
			    		 $("#theaterName").append("<option value='"+result[i].theaterName+"'>"+result[i].theaterName+"</option>");
			    		 $("#theaterNum").append("<option value='"+result[i].theaterNum+"'>"+result[i].theaterNum+"</option>");	
	
					}
			    } 
			});
			
		});
		
		
	});
</script>
<script>
$(function(){
	var seatId;
	$('#sDate').change(function(){
		seatId = this.value;
		$.ajax({
			url : "${pageContext.request.contextPath}/movie/seats.do",
			async: true, 
		    type: 'GET' ,
		    data: {
		    	seatId : seatId
		    }, 
		    dataType: 'json',
		    success: function(result1) {
		    	console.log(result1)
		 
		    	for (var j = 0; j < result1.length; j++) {
		    		 $("#seatName").append("<option value='"+result1[j].seatName+"'>"+result1[j].seatName+"</option>");
		    		 $("#seatType").append("<option value='"+result1[j].seatType+"'>"+result1[j].seatType+"</option>");
		    		 
				}
		    } 
		});
	});

	$.ajax({
		url : "${pageContext.request.contextPath}/movie/seatsTest.do",
		async: true, 
	    type: 'GET' ,
	    dataType: 'json',
	    success: function(result) {
	    	$('#seats > tbody:last').append('<tr>');
	    	for(var i = 0; i < 4; i++){
 	    		 $('#seats > tbody:last').append('<td><input type="checkbox" name="seat" value="'+result[i].seatType+result[i].seatName+'">'+result[i].seatType+result[i].seatName+'</td>');
	    	}
	    	$('#seats > tbody:last').append('</tr>');
	    	
	    	$('#seats > tbody:last').append('<tr>');
	    	for(var i = 4; i < 8; i++){
	    		$('#seats > tbody:last').append('<td><input type="checkbox" name="seat" value="'+result[i].seatType+result[i].seatName+'">'+result[i].seatType+result[i].seatName+'</td>');
	    	}
	    	$('#seats > tbody:last').append('</tr>');
	    	
	    	$('#seats > tbody:last').append('<tr>');
	    	for(var i = 8; i < 12; i++){
	    		$('#seats > tbody:last').append('<td><input type="checkbox" name="seat" value="'+result[i].seatType+result[i].seatName+'">'+result[i].seatType+result[i].seatName+'</td>');
	    	}
	    	$('#seats > tbody:last').append('</tr>');
	    	
	    	$('#seats > tbody:last').append('<tr>');
	    	for(var i = 12; i < 16; i++){
	    		$('#seats > tbody:last').append('<td><input type="checkbox" name="seat" value="'+result[i].seatType+result[i].seatName+'">'+result[i].seatType+result[i].seatName+'</td>');
	    	}
	    	$('#seats > tbody:last').append('</tr>');
	    } 
	});
});

</script> 
 

<title>내 정보</title>



</head>
<body>
	<div class="container">
		<form action="${pageContext.request.contextPath}/reserve/reserve.do" method="post">
		<div>
			<table class="table table-striped">
				<thead>
					<tr>
						<th width="20%">상영 시간</th>
						<th width="20%">상영 날짜</th>
						<th width="20%">상영 지역</th>
						<th width="10%">상영 극장</th>
						<th width="10%">상영 번호</th>
						<th width="10%">좌석 타입</th>
						<th width="10%">좌석 번호</th>
					</tr>
				</thead>
				<tbody>
						<tr>
							<td>
								<select name="sDate" id="sDate">
									<option value="">선택</option>
									<c:forEach var="movie" items="${movies}" >
										<option value="<c:out value="${movie.sDate }" />" id="${movie.movieId }">${movie.sDate } </option>
									</c:forEach>
								</select>
							</td>
							<td>
								<select name="eDate" id="eDate">
									<option value="">선택</option>
									<c:forEach var="movie" items="${movies}" >
										<option value="<c:out value="${movie.eDate }" />" id="${movie.movieId }">${movie.eDate } </option>
									</c:forEach>
								</select>
							</td>
							<td>
								<select name="theaterArea" id="theaterArea">
									<option value="">선택</option>
								</select>
							</td>
						<td>
								<select name="theaterName" id="theaterName">
									<option value="">선택</option>
								</select> 
							</td> 
							<td>
								<select name="theaterNum" id="theaterNum">
									<option value="">선택</option>
								</select> 
							</td> 
							<td>
								<select name="seatType" id="seatType">
									<option value="">선택</option>
								</select> 
							</td> 
							<td>
								<select name="seatName" id="seatName">
									<option value="">선택</option>
								</select> 
							</td> 
						</tr>
				</tbody>
			</table>
		</div>
		<br>
		<br>
		<br>
		<br>
		<div>
			<table class="table table-sm table-dark" id="seats">
  <thead>
  	<tr>
  		<th colspan="4" style="text-align: center;">SCREEN</th>
  	</tr>
  </thead>
  <tbody>
  </tbody>
</table>
		</div>
		
		<br>
		<br>
		<br>
		<br>
		<br>
		<button type="submit">예약하기</button>
		</form>
	</div>
</body>
</html>
