<!DOCTYPE html>
<html>
<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<title>PLAN</title>

<meta charset="utf-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="${pageContext.request.contextPath}/resources/js/jquery-2.1.3.js"></script>
<script>
$(function(){
	var oldVal;
	$("#reservePeople").on("propertychange change keyup paste input", function() {
	    var currentVal = $(this).val();
	    if(currentVal == oldVal) {
	        return;
	    }
	 
	    oldVal = currentVal;
	    $("#reservePay").val(oldVal * 7000);
	});
});
</script>
<style type="text/css">
</style>
</head>
<body>


	<div class="container">
		<form action="${pageContext.request.contextPath}/reserve/pay.do"
			method="post">
			<input type="hidden" name="eDate" value="${list.eDate }"/>
			<input type="hidden" name="sDate" value="${list.sDate }"/>
			<input type="hidden" name="theaterArea" value="${list.theaterArea}"/>
			<input type="hidden" name="theaterName" value="${list.theaterName}"/>
			<input type="hidden" name="theaterNum" value="${list.theaterNum }"/>
			<input type="hidden" name="seat" value="${list.seat }"/>
			
			<fieldset>
				<table class="table">

					<tr>
						<th>eDateTime</th>
						<td><input id="reserveDate" name="reserveDate"
							class="form-control" type="date" value="" placeholder="상영시간">
						</td>
					</tr>
					<tr>
						<th>reservePeople</th>
						<td><input id="reservePeople" name="reservePeople"
							class="form-control" type="number" value="" placeholder="인원"></td>
					</tr>
					<tr>
						<th>reservePay</th>
						<td><input id="reservePay" name="reservePay"
							class="form-control" type="number" value="" placeholder="인원" readonly="readonly">
<!-- 						<select name="reservePay" id="reservePay">
							<option value="">선택</option>
							<option value="7000">7000</option>
							<option value="14000">14000</option>
							<option value="21000">21000</option>
							<option value="28000">28000</option>
						</select> -->
						</td>
				<!-- 		<th>reservePay</th>
						<td><input id="reservePay" name="reservePay"
							class="form-control" type="text" value="" placeholder="가격"></td> -->
					</tr>
					
					
					<!-- 	<tr>
						<th>endDate</th>
						<td><input id="eDate" name="eDate" class="form-control"
							type="datetime-local" value="" placeholder="종료날짜"></td>
					</tr> -->
				</table>
				<br>
				<div align="center">
					<input class="btn" type="reset" value="취소"> <input
						class="btn btn-success" type="submit" value="확인">
				</div>
			</fieldset>
		</form>
	</div>

	<br>
</body>
</html>
