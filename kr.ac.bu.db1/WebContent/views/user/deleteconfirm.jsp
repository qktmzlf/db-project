<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>내 정보</title>

<style>

.table {
	width: 900px;
}


.table-plan {
	margin-top: 40px;
}

table {
	border-collapse: separate;
	border-spacing: 0 15px;
}

.modify-input {
	position: absolute;
	left: 50%;
	top: 30%;
	transform: translate(-50%, -60%);
}

.footer {
	position: absolute;
	left: 0;
	bottom: 0;
	right: 0;
	margin-bottom: 20px;
}

</style>

</head>
<body>
<nav class="navbar navbar-light" style="background-color: #e3f2fd;">
		<a class="navbar-brand" href="${pageContext.request.contextPath}/views/calendar/cal.jsp">Planner</a>
	</nav>
	<div class="container">
	</div>
	<div>
		<center class="mt-5">회원 탈퇴를 위해 비밀번호를 입력해주세요.</center>
	</div>
	<div class="modify-input">
		<form action="${pageContext.request.contextPath}/user/delete.do"
			method="POST" class="form-signin">
			<tr>${PWFail }</tr>
			<input type="password" class="form-control" name="userPw"
				placeholder="비밀번호" required />
			<center>
				<div>
					<button class="btn btn-danger mt-2">탈퇴하기</button>
				</div>
			</center>
		</form>
	</div>
	<footer class="container footer">
	<a href="${pageContext.request.contextPath}/user/information.do">마이페이지</a>
	</footer>
</body>
</html>

