<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<style>
#menu {
	height: 40px;
	width: 1000px;
	left: 0;
	right: 0;
	bottom: 0;
	margin-bottom: 30px;
	margin-left: -39px;
	margin-bottom: 30px
}

#menu ul li {
	list-style: none;
	color: white;
	float: left;
	line-height: 40px;
	vertical-align: middle;
	text-align: center;
}

#menu .menu-link {

	background-color: #00C6ED;
	text-decoration: none;
	color: white;
	display: block;
	width: 150px;
	font-weight: bold;
	font-family: "Trebuchet MS", Dotum, Arial;
}

#menu .menu-link:hover {
	background-color: #12EAFF;
}
</style>

</head>
<body>
	<div id="menu">
		<ul> <!--  버튼  -->
			<li><a class="btn btn-outline-warning"
				href="${pageContext.request.contextPath}/views/user/modify.jsp">회원정보수정/탈퇴</a></li>
			<li><a class="btn btn-outline-info ml-2"
				href="${pageContext.request.contextPath}/user/logout.do">로그아웃</a></li>

		</ul>
	</div>
</body>
</html>
