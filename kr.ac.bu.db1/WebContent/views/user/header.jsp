<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<style>

</style>

</head>
<body>
	<nav class="navbar navbar-light sticky-top" style="background-color: #e3f2fd;">
		<a class="navbar-brand">Planner</a>
		<ul class="nav">
			<li class="nav-item"><a class="nav-link active"
				href="${pageContext.request.contextPath}/views/calendar/cal.jsp">home</a>
			</li>
			<li class="nav-item"><a class="nav-link"
				href="${pageContext.request.contextPath}/views/plan/plan.jsp">plan</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${pageContext.request.contextPath}/board/boardList.do">list</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${pageContext.request.contextPath}/group/groupList.do">group</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${pageContext.request.contextPath}/user/information.do"
				tabindex="-1" aria-disabled="true">mypage</a></li>
		</ul>
	</nav>
</body>
</html>
