<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>내 정보</title>



</head>
<body>
<%@ include file="/views/user/header.jsp"%>
	<div class="container">
		
		<div>
			<table class="table table-striped">
				<thead>
					<tr>
						<th width="20%">티켓 번호</th>
						<th width="50%">영화 제목</th>
						<th width="30%">상영 시간</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<c:forEach items="${reserves }" var="reserve">
							<tr>
								<td><a
									href="${pageContext.request.contextPath}/reserve/detail.do?reserveId=${reserve.reserveId}">${reserve.reserveId}</a></td>
								<td>${reserve.movieName}</td>
								<td>${reserve.sDate} ${reserve.sTime }</td>
							</tr>
						</c:forEach>
				</tbody>
			</table>
		</div>
		
	</div>
</body>
</html>
