<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>내 정보</title>

<style>
.container {
	width: 900px;
	margin: auto;
}

.table {
	width: 900px;
}

.btn-modify {
	margin: 5%;
	background-color: #00C6ED;
	color: white;
	font-weight: bold;
	font-family: "Trebuchet MS", Dotum, Ar;
	background-color: #00C6ED;
	background-color: #00C6ED;
}

.btn-modify:hovor {
	background-color: #12EAFF;
	color: white !important;
}

.table-plan {
	margin-top: 40px;
}

table {
	border-collapse: separate;
	border-spacing: 0 15px;
}

th {
	padding-right: 10px;
}

.modify-input {
	position: absolute;
	left: 50%;
	top: 40%;
	transform: translate(-50%, -60%);
}

.btn-align {
	margin: auto;
}

.modify-align {
	font-size: 30px;
	margin-bottom: 30px;
}

.footer {
	position: absolute;
	left: 0;
	bottom: 0;
	right: 0;
	margin-bottom: 20px;
}
</style>

</head>
<body>
	<nav class="navbar navbar-light" style="background-color: #e3f2fd;">
		<a class="navbar-brand" href="${pageContext.request.contextPath}/views/calendar/cal.jsp">Planner</a>
	</nav>
	
	
	<div class="container">
	</div>

	<div class="modify-input">
		<form action="${pageContext.request.contextPath}/user/modify.do"
			method="POST" class="form-signin">
			<center class="modify-align">개인정보수정</center>
			<table>
				<tr>
					<th>ID</th>
					<td>${user.userId }</td>
					<td><input id="userId" name="userId" value="${user.userId}"
						class="form-control" type="hidden" readonly="readonly"></td>
				</tr>
				<tr>
					<th>Password</th>
					<td><input id="userPw" name="userPw" class="form-control"
						type="password" placeholder="비밀번호 입력" required></td>
				</tr>
				<tr>
					<th>Name</th>
					<td><input id="userName" name="userName"
						value="${user.userName}" class="form-control" type="text" required></td>
				</tr>
				<tr>
					<th>Phone</th>
					<td><input id="userPhone" name="userPhone"
						value="${user.userPhone}" class="form-control" type="text"
						required></td>
				</tr>
			</table>
			<center>
				<button class="btn btn-outline-success" type="submit" value="수정">수정</button>
				<button class="btn btn-outline-warning ml-3"
					onclick="location.href='${pageContext.request.contextPath}/views/user/deleteconfirm.jsp'">탈퇴</button>
			</center>
		</form>
		
		
	</div>
	<footer class="container footer">
	<a href="${pageContext.request.contextPath}/user/information.do">마이페이지</a>
	</footer>

</body>
</html>
