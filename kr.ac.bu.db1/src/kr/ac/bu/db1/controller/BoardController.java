package kr.ac.bu.db1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kr.ac.bu.db1.domain.Reserve;
import kr.ac.bu.db1.service.BoardService;

@Controller
@RequestMapping("board")
public class BoardController {

	@Autowired
	private BoardService service;

	@RequestMapping(value = "boardList.do", method = RequestMethod.GET)
	public String ListAll(Model model) throws Exception {

		List<Reserve> list = service.findAll(); // plan �� list �� �ްڴ�.
		model.addAttribute("list", list);

		return "board/boardList";

	}
		
	
}
