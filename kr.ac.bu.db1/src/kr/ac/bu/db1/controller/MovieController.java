package kr.ac.bu.db1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.bu.db1.domain.Movie;
import kr.ac.bu.db1.domain.Seat;
import kr.ac.bu.db1.domain.Theater;
import kr.ac.bu.db1.service.MovieService;


@Controller
@RequestMapping("movie")
public class MovieController {

	@Autowired
	private MovieService service;
	
	@RequestMapping(value="/findAll.do")
	public ModelAndView showBoardList() {
		
		List<Movie> list = service.findAll();
		
		ModelAndView modelAndView = new ModelAndView("movie/movie");
		modelAndView.addObject("MovieList", list);
		
		return modelAndView;
		
	}
	
	
	@RequestMapping(value="/searchByName.do")
	public ModelAndView searchByName(@RequestParam("movieName") String name) {
		
		List<Movie> list = service.findByName(name);
		ModelAndView modelAndView = new ModelAndView("movie/movie");
		modelAndView.addObject("MovieList", list);
		
		return modelAndView;
		
	}
	
	@RequestMapping(value="/read.do")
	public ModelAndView showDetail(@RequestParam("movieId") int id) {
		ModelAndView modelAndView = new ModelAndView("movie/movie");
		modelAndView.addObject("movie", service.find(id));
		
		return modelAndView;
		
	}
	//
	@RequestMapping(value="/movies.do" )
	public ModelAndView movieList(){
		ModelAndView modelAndView = new ModelAndView("movie/movies");
		modelAndView.addObject("movieList", service.findMovieAll());
		
		return modelAndView;
		
	}
	@RequestMapping(value="/movie.do")  //상영관
	public ModelAndView movie(@RequestParam("name") String movie) {
		ModelAndView modelAndView = new ModelAndView("movie/theater2");
		modelAndView.addObject("movies", service.findByMovie(movie)); // 영화(영화 시간, 연령대 등)
//		modelAndView.addObject("theaters", service.findTheater()); // 영화관
//		modelAndView.addObject("seats", service.findSeat());// 좌석
		return modelAndView;
	}
	
	@RequestMapping(value="/theaters.do",  method= RequestMethod.GET) //극장
	public @ResponseBody List<Theater> theaters(@RequestParam("movieId") String movieId ) {
//		System.out.println(param);
//		ModelAndView modelAndView = new ModelAndView("movie/theater2");
//		modelAndView.addObject("theaters", service.findTheater(param)); // 영화관
		return service.findTheater(movieId);
	}

	// 나중에 하기
//	@RequestMapping(value="/theaters.do",  method= RequestMethod.GET) //극장
//	public ModelAndView theaters(@RequestParam("movieId") String movieId) {
//		ModelAndView modelAndView = new ModelAndView("movie/theater2");
//		modelAndView.addObject("theaters", service.findTheater(movieId)); // 영화관
//		return modelAndView;
//	}
	@RequestMapping(value="/seats.do", method=RequestMethod.GET) //좌석
	public @ResponseBody List<Seat> seats(@RequestParam("seatId") String seatId) {
		System.out.println(seatId);
//		ModelAndView modelAndView = new ModelAndView("movie/theater2");
//		modelAndView.addObject("seats", service.findSeat(seatId)); // 영화(영화 시간, 연령대 등)
		return service.findSeat(seatId);
	}
	
	@RequestMapping(value="/seatsTest.do", method=RequestMethod.GET) //좌석
	public @ResponseBody List<Seat> seatsTest() {
		return service.findSeatTest();
	}
	
	@RequestMapping("detail")
	public ModelAndView Detail(@RequestParam("movieId") String movieId) {
		ModelAndView modelAndView = new ModelAndView("movie/detail");
		modelAndView.addObject("movie", service.find(Integer.parseInt(movieId)));
		return modelAndView;
	}
}
