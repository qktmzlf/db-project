package kr.ac.bu.db1.controller;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.bu.db1.domain.Reserve;
import kr.ac.bu.db1.domain.Reserve2;
import kr.ac.bu.db1.domain.User;
import kr.ac.bu.db1.service.ReserveService;

@Controller
@RequestMapping("reserve")
public class ReserveController {
	
	
	@Autowired
	private ReserveService service;
	
	@RequestMapping(value="reserve.do", method = RequestMethod.POST)
	public ModelAndView MakeReserve(Reserve2 reserve, HttpSession session) {
		
		
		ModelAndView modelAndView = new ModelAndView("reserve/reserve");
		modelAndView.addObject("list", reserve);
//		System.out.println("reserve.geteDate() : "+reserve.geteDate());
//		System.out.println("reserve.getsDate() : "+reserve.getsDate());
//		System.out.println("reserve.getTheaterArea() : "+reserve.getTheaterArea());
		System.out.println("reserve.do getTheaterName() : "+reserve.getTheaterName());
//		System.out.println("reserve.getTheaterNum() : "+reserve.getTheaterNum());
//		System.out.println("reserve.getSeat() : "+reserve.getSeat());
		reserve.setUserId("123");
//		service.create(reserve);
		session.setAttribute("theaterName", reserve.getTheaterName());
		return modelAndView;

	}
	
	@RequestMapping(value="pay.do", method = RequestMethod.POST)
	public String MakeReservePay(Reserve2 reserve2,Reserve reserve, HttpSession session) {
		User user = new User();
		user = (User) session.getAttribute("user");
		String userId = user.getUserId();
		
		reserve2.setTheaterName((String)session.getAttribute("theaterName"));
//		System.out.println("reserve.geteDate() : "+reserve2.geteDate());
//		System.out.println("reserve.getsDate() : "+reserve2.getsDate());
//		System.out.println("reserve.getTheaterArea() : "+reserve2.getTheaterArea());
		System.out.println("pay.do getTheaterName() : "+reserve2.getTheaterName());
//		System.out.println("reserve.getTheaterNum() : "+reserve2.getTheaterNum());
//		System.out.println("reserve.getSeat() : "+reserve2.getSeat());
		reserve.setUserId(user.getUserId());
//		System.out.println("=======================================");
//		System.out.println("reserve.getReserveDate() : "+reserve.getReserveDate());
//		System.out.println("reserve.getReservePeople() : "+reserve.getReservePeople());
//		System.out.println("reserve.getReservePay() : "+reserve.getReservePay());
		
		service.reserve(reserve, reserve2);
		
		
		return "redirect:/user/info.do";

	}
	
//	@RequestMapping(value="reserve.do", method = RequestMethod.POST)
//	public String MakeReserve(Reserve reserve, String userId, HttpSession session) {
//		
//		
//		reserve.setUserId("123");
//		System.out.println("controller" + reserve.getReservePeople());
//		
//		service.create(reserve);
//		
//		return "redirect:/movie/movies.do";
//
//	}
	
	
	
	
	

}
