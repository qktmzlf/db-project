package kr.ac.bu.db1.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.bu.db1.domain.User;
import kr.ac.bu.db1.service.UserService;

@Controller
@RequestMapping("user")
public class UserController {

	@Autowired
	private UserService service;

	@RequestMapping(value = "login.do", method = RequestMethod.POST)
	public String login(Model model, HttpSession session, User user) {

		user = service.login(user);

		if (user.getUserPw() == null) {
			model.addAttribute("loginFail", "ID/PW확인하세요");
			return "common/login";
		} else if (user.getUserPw().equals(user.getUserPw())) {
			session.setAttribute("user", user);
			return "redirect:/movie/movies.do";
		} else {
			model.addAttribute("loginFail", "ID/PW 확인하세요");
			return "common/login";
		}

	}

	@RequestMapping(value = "delete.do", method = RequestMethod.POST)
	public String delete(Model model, HttpSession session, String userPw) {
		User user = new User();
		user = (User) session.getAttribute("user");
		String userId = user.getUserId();

		if (service.delete(userId, userPw)) {
			session.setAttribute("user", null);
			return "user/login";
		} else {
			return "user/deleteconfirm";
		}
	}

	@RequestMapping(value = "modify.do")
	public String modify(Model model, HttpSession session) {
		User user = new User();
		user = (User) session.getAttribute("user");
		model.addAttribute("users", user);
		return "common/modify";
	}

	@RequestMapping(value = "modify.do", method = RequestMethod.POST)
	public String modify(Model model, HttpSession session, User user) {

		System.out.println("modify" + user.getUserId());
		if (service.modify(user)) {
			return "redirect:/user/information.do";
		} else
			return "redirect:/user/modify.do";
	}

	@RequestMapping(value = "logout.do")
	public String logout(HttpSession session) {
		session.invalidate();
		return "user/login";
	}

	@RequestMapping(value = "join.do")
	public String join(Model model) {

		model.addAttribute("joinFail", "ȸ������ ��Ź�帳�ϴ�.");

		return "user/join";
	}

	@RequestMapping(value = "join.do", method = RequestMethod.POST)
	public String join(String userId, String userPw, String userName, String userPhone, User user) {

		user.setUserId(userId);
		user.setUserPw(userPw);
		user.setUserName(userName);
		user.setUserPhone(userPhone);

		try {
			service.register(user);
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/user/join.do";
		}
		return "user/login";

	}
	
	@RequestMapping("info.do")
	public ModelAndView info(HttpSession session) {
		User user = (User)session.getAttribute("user");
		String userId = user.getUserId();
		
		
		ModelAndView modelAndView = new ModelAndView("user/information");
		
		modelAndView.addObject("reserves", service.info(userId));
		return modelAndView;
	}
}
