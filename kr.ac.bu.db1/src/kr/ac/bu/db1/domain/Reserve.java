package kr.ac.bu.db1.domain;

public class Reserve {

	
	private int reserveId;
	private String reserveDate;
	private int reservePeople;
	private int reservePay;
	private String sDate;
	private String sTime;
	private String movieId;
	private String movieName;
	private String userId;
	
	public int getReserveId() {
		return reserveId;
	}
	public void setReserveId(int reserveId) {
		this.reserveId = reserveId;
	}
	public String getReserveDate() {
		return reserveDate;
	}
	public void setReserveDate(String reserveDate) {
		this.reserveDate = reserveDate;
	}
	public int getReservePeople() {
		return reservePeople;
	}
	public void setReservePeople(int reservePeople) {
		this.reservePeople = reservePeople;
	}
	public int getReservePay() {
		return reservePay;
	}
	public void setReservePay(int reservePay) {
		this.reservePay = reservePay;
	}
	public String getsDate() {
		return sDate;
	}
	public void setsDate(String sDate) {
		this.sDate = sDate;
	}
	public String getsTime() {
		return sTime;
	}
	public void setsTime(String sTime) {
		this.sTime = sTime;
	}
	public String getMovieId() {
		return movieId;
	}
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	

}
