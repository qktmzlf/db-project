package kr.ac.bu.db1.domain;

public class Seat {

	private String seatType;
	private int seatName;
	private int seatId;

	public String getSeatType() {
		return seatType;
	}

	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}

	public int getSeatName() {
		return seatName;
	}

	public void setSeatName(int seatName) {
		this.seatName = seatName;
	}

	public int getSeatId() {
		return seatId;
	}

	public void setSeatId(int seatId) {
		this.seatId = seatId;
	}

}
