package kr.ac.bu.db1.domain;

public class Theater {

	private String theaterName;
	private String theaterArea;
	private String theaterNum;
	private int movieId;
	private int seatId;

	public String getTheaterName() {
		return theaterName;
	}

	public void setTheaterName(String theaterName) {
		this.theaterName = theaterName;
	}

	public String getTheaterArea() {
		return theaterArea;
	}

	public void setTheaterArea(String theaterArea) {
		this.theaterArea = theaterArea;
	}

	public String getTheaterNum() {
		return theaterNum;
	}

	public void setTheaterNum(String theaterNum) {
		this.theaterNum = theaterNum;
	}

	public int getMovieId() {
		return movieId;
	}

	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}
	
	public int getSeatId() {
		return seatId;
	}

	public void setSeatId(int seatId) {
		this.seatId = seatId;
	}

}
