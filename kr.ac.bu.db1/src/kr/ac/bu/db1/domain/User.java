package kr.ac.bu.db1.domain;


import java.sql.Date;
import java.util.List;

public class User {

	private String userId;
	private String userPw;
	private String userName;
	private String userPhone;
	private Date userBirth;
	private String userAddr;
	private String memberId;
	private int point;
	private List<Reserve> reserve;
	private List<Movie> movie;
	
	
	public List<Movie> getMovie() {
		return movie;
	}
	public void setMovie(List<Movie> movie) {
		this.movie = movie;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserPw() {
		return userPw;
	}
	public void setUserPw(String userPw) {
		this.userPw = userPw;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public Date getUserBirth() {
		return userBirth;
	}
	public void setUserBirth(Date userBirth) {
		this.userBirth = userBirth;
	}
	public String getUserAddr() {
		return userAddr;
	}
	public void setUserAddr(String userAddr) {
		this.userAddr = userAddr;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public int getPoint() {
		return point;
	}
	public void setPoint(int point) {
		this.point = point;
	}
	public List<Reserve> getReserve() {
		return reserve;
	}
	public void setReserve(List<Reserve> reserve) {
		this.reserve = reserve;
	}
	
	
	
	
	
	
}