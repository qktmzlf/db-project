package kr.ac.bu.db1.service;

import java.util.List;

import kr.ac.bu.db1.domain.Movie;
import kr.ac.bu.db1.domain.Seat;
import kr.ac.bu.db1.domain.Theater;

public interface MovieService {

	List<Movie> findMovie(String movie);
	List<Movie> findMovieAll();
	List<Theater> findTheater(String movieId );
	List<Seat> findSeat(String seatId);
	List<Seat> findSeatTest();
	Movie find(int movieId);
	List<Movie> findAll();
	List<Movie> findByName(String name);

	List<Movie> findByMovie(String movie);
}
