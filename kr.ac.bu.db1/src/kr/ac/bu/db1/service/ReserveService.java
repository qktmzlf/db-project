package kr.ac.bu.db1.service;

import kr.ac.bu.db1.domain.Reserve;
import kr.ac.bu.db1.domain.Reserve2;


public interface ReserveService {

	void create(Reserve reserve);
//	void create(Reserve2 reserve);
	void reserve(Reserve reserve, Reserve2 reserve2);
	
}

