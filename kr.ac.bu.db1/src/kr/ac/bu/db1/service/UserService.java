package kr.ac.bu.db1.service;

import java.util.List;

import kr.ac.bu.db1.domain.Reserve;
import kr.ac.bu.db1.domain.User;

public interface UserService {
	//
	User login(User user);
	boolean register(User user);
	boolean delete(String userId, String userPw);
	boolean modify(User user);
	List<Reserve> info(String userId);
}
