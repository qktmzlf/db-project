package kr.ac.bu.db1.service.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.ac.bu.db1.domain.Movie;
import kr.ac.bu.db1.domain.Seat;
import kr.ac.bu.db1.domain.Theater;
import kr.ac.bu.db1.service.MovieService;
import kr.ac.bu.db1.store.MovieStore;


@Service
public class MovieServiceLogic implements MovieService{

	@Autowired
	private MovieStore store;
	
	
	@Override
	public List<Movie> findAll() {
		// TODO Auto-generated method stub
		return store.findAll();
	}


	@Override
	public List<Movie> findMovieAll() {
		// TODO Auto-generated method stub
		return store.findMovieAll();
	}


	@Override
	public List<Movie> findByName(String name) {
		// TODO Auto-generated method stub
		return store.readByName(name);
	}




	@Override
	public List<Movie> findMovie(String movie) {
		// TODO Auto-generated method stub
		return store.findMovie(movie);
	}


	@Override
	public List<Theater> findTheater( String movieId) {
		return store.findTheater(movieId);
	}


	@Override
	public List<Seat> findSeat(String seatId) {
		System.out.println(seatId);
		
		return store.findSeat(seatId);
	}
	
	@Override
	public List<Seat> findSeatTest() {
		return store.findSeatTest();
	}
	
	@Override
	public Movie find(int movieId) {
		// TODO Auto-generated method stub
		return store.select(movieId);
	}


	@Override
	public List<Movie> findByMovie(String movie) {
		return store.findByMovie(movie);
	}








}
