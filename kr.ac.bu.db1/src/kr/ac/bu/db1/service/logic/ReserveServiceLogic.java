package kr.ac.bu.db1.service.logic;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.ac.bu.db1.domain.Reserve;
import kr.ac.bu.db1.domain.Reserve2;
import kr.ac.bu.db1.service.ReserveService;
import kr.ac.bu.db1.store.ReserveStore;

@Service
public class ReserveServiceLogic implements ReserveService{

	@Autowired
	private ReserveStore store;
	
	
	@Override
	public void create(Reserve reserve) {
		System.out.println("service" + reserve.getReservePeople());
		store.create(reserve);
		
	}
	
	@Override
	public void reserve(Reserve reserve, Reserve2 reserve2) {
		
		store.reserve(reserve, reserve2);
		
	}
	

	/*
	 * @Override public void create(Reserve2 reserve) { store.create(reserve); }
	 */

}
