package kr.ac.bu.db1.service.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.ac.bu.db1.domain.Reserve;
import kr.ac.bu.db1.domain.User;
import kr.ac.bu.db1.service.UserService;
import kr.ac.bu.db1.store.UserStore;

@Service
public class UserServiceLogic implements UserService {

	@Autowired
	private UserStore store;

	@Override
	public User login(User user) {

		user = store.login(user);

		return user;
	}

	@Override
	public boolean register(User user) {

		System.out.println("service " + user.getUserId());

		store.create(user, store.create()); // memberID?�� 값을 ?��?��링으�? ?��번째 ?��?��미터?�� ?��?���??��.

		return false;
	}

	@Override
	public boolean delete(String userId, String userPw) {
		return store.delete(userId, userPw);
	}

	@Override
	public boolean modify(User user) {
		return store.modify(user);
	}

	@Override
	public List<Reserve> info(String userId) {
		

		return store.info(userId);
	}

}