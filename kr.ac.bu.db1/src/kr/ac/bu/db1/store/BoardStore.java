package kr.ac.bu.db1.store;

import java.util.List;

import kr.ac.bu.db1.domain.Board;
import kr.ac.bu.db1.domain.Reserve;

public interface BoardStore {
	
	List<Reserve> findAll();
	
}
