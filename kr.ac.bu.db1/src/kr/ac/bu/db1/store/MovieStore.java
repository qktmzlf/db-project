package kr.ac.bu.db1.store;

import java.util.List;

import kr.ac.bu.db1.domain.Movie;
import kr.ac.bu.db1.domain.Seat;
import kr.ac.bu.db1.domain.Theater;

public interface MovieStore {

	List<Movie> findMovie(String movie);
	List<Movie> findAll();
	List<Movie> findMovieAll();
	List<Theater> findTheater( String movieId);
	List<Seat> findSeat(String seatId);
	List<Seat> findSeatTest();
	Movie read(int id);
	List<Movie> readByName(String name);
	List<Movie> findByMovie(String movie);
	Movie select(int movieId);
}
