package kr.ac.bu.db1.store;

import kr.ac.bu.db1.domain.Reserve;
import kr.ac.bu.db1.domain.Reserve2;

public interface ReserveStore {
	
	
	void create(Reserve reserve);
	void reserve(Reserve reserve, Reserve2 reserve2);

}
