package kr.ac.bu.db1.store;

import java.util.List;

import kr.ac.bu.db1.domain.Reserve;
import kr.ac.bu.db1.domain.User;

public interface UserStore {
	//
	boolean create(User user, int meberId);
	int create();
	User login(User user);
	boolean delete(String userId, String userPw);
	boolean modify(User user);
	List<Reserve> info(String userId);
		
}
