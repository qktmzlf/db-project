package kr.ac.bu.db1.store.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import kr.ac.bu.db1.domain.Board;
import kr.ac.bu.db1.domain.Reserve;
import kr.ac.bu.db1.store.BoardStore;
import kr.ac.bu.db1.store.factory.ConnectionFactory;
import kr.ac.bu.db1.store.logic.util.JdbcUtils;

@Repository
public class BoardStoreLogic implements BoardStore {

	private ConnectionFactory connectionFactory;

	public BoardStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Override
	public List<Reserve> findAll() {
		// TODO Auto-generated method stub
		List<Reserve> list = new ArrayList<Reserve>();
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;

		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement("SELECT * FROM reservetbl");

			rs = psmt.executeQuery();
			while (rs.next()) {
				Reserve reserve = new Reserve();

				reserve.setsDateTime(rs.getString(1));
				reserve.seteDateTime(rs.getString(2));
				reserve.setReservePeople(rs.getInt(3));

				list.add(reserve); // 플랜을 리스트에 저장
			}
			rs.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, connection);
		}
		return list;
	}
	
	
	
}
