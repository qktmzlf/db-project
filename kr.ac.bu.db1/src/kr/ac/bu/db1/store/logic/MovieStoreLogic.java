package kr.ac.bu.db1.store.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import kr.ac.bu.db1.domain.Movie;
import kr.ac.bu.db1.domain.Seat;
import kr.ac.bu.db1.domain.Theater;
import kr.ac.bu.db1.store.MovieStore;
import kr.ac.bu.db1.store.factory.ConnectionFactory;
import kr.ac.bu.db1.store.logic.util.JdbcUtils;

@Repository
public class MovieStoreLogic implements MovieStore {

	private ConnectionFactory connectionFactory;

	public MovieStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Override
	public List<Movie> findAll() {
		String sql="select * from movietbl";
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<Movie> movies = new ArrayList<Movie>();

		try {
			conn = connectionFactory.createConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
				Movie movie = new Movie();
		
				movie.setMovieId(rs.getInt(1));
				movie.setName(rs.getString(2));
				movie.setsDate(rs.getString(3));
				movie.seteDate(rs.getString(4));
				movie.setAge(rs.getString(5));
				movie.setGenre(rs.getString(6));
				movie.setDir(rs.getString(7));
				movie.setActor(rs.getString(8));
				movie.setImg(rs.getString(9));
				movies.add(movie); // �÷��� ����Ʈ�� ����
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, stmt, conn);
		}
		return movies;
	}

	@Override
	public Movie read(int id) {
		String sql = "select * from movietbl where movieid = ?";
		
		Connection conn=null;
		PreparedStatement psmt = null;
		ResultSet rs= null;
		Movie movie = null;
		
		try {
			conn=connectionFactory.createConnection();
			psmt=conn.prepareStatement(sql);
			psmt.setInt(1, id);
			rs = psmt.executeQuery();
			if(rs.next()) {
				movie = new Movie();
				
				movie.setMovieId(rs.getInt(1));
				movie.setName(rs.getString(2));
				movie.setsDate(rs.getString(3));
				movie.seteDate(rs.getString(4));
				movie.setAge(rs.getString(5));
				movie.setGenre(rs.getString(6));
				movie.setDir(rs.getString(7));
				movie.setActor(rs.getString(8));
				movie.setImg(rs.getString(9));
				
			}
		}catch (Exception e) {
			throw new RuntimeException(e);
		}finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return movie;
	}

	@Override
	public List<Movie> readByName(String name) {
		String sql = "SELECT * FROM MOVIETBL WHERE MOVIENAME LIKE ?";
		
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Movie> movies = new ArrayList<Movie>();
		
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, "%"+name+"%");
			rs = psmt.executeQuery();
			
			while(rs.next()) {
				Movie movie = new Movie();
				
				movie.setMovieId(rs.getInt(1));
				movie.setName(rs.getString(2));
				movie.setsDate(rs.getString(3));
				movie.seteDate(rs.getString(4));
				movie.setAge(rs.getString(5));
				movie.setGenre(rs.getString(6));
				movie.setDir(rs.getString(7));
				movie.setActor(rs.getString(8));
				movie.setImg(rs.getString(9));
				movies.add(movie);
			
			}
		}catch (Exception e) {
			throw new RuntimeException(e);
		}finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		
		return movies;
	}

	@Override
	public List<Movie> findMovieAll() {
		String sql="select movieName, Image, movieId from movietbl group by movieName";
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<Movie> movies = new ArrayList<Movie>();

		try {
			conn = connectionFactory.createConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
				Movie movie = new Movie();
				movie.setName(rs.getString(1));
				movie.setImg(rs.getString(2));
				movie.setMovieId(rs.getInt(3));
				
				movies.add(movie);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, stmt, conn);
		}
		return movies;
	}

	@Override
	public List<Movie> findMovie(String movieName) {
		String sql="select * from movietbl where moviename = ?";
		
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Movie> movies = new ArrayList<Movie>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, movieName);
			rs = psmt.executeQuery();
			
			while (rs.next()) {
				Movie movie = new Movie();
				movie.setName(rs.getString(1));
				movie.setImg(rs.getString(2));
				movies.add(movie);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return movies;
	}

	@Override
	public List<Theater> findTheater(String movieId) {
		String sql="select * from theatertbl where movieId = ?";
		
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Theater> theaters = new ArrayList<Theater>();

		try {
			conn = connectionFactory.createConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, movieId);
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				Theater theater = new Theater();
				theater.setTheaterName(rs.getString(1));
				theater.setTheaterArea(rs.getString(2));
				theater.setTheaterNum(rs.getString(3));
				theater.setMovieId(rs.getInt(4));
				theater.setSeatId(rs.getInt(5));
				theaters.add(theater);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, stmt, conn);
		}
		return theaters;
	}

	@Override
	public List<Seat> findSeat(String seatId) {
		String sql="select * from seattbl where seatName = ?";
		
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Seat> seats = new ArrayList<Seat>();
		System.out.println(seatId);

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, seatId);
			rs = psmt.executeQuery();
			
			while (rs.next()) {
				Seat seat = new Seat();
				seat.setSeatId(rs.getInt(1));
				seat.setSeatName(rs.getInt(2));
				seat.setSeatType(rs.getString(3));
				seats.add(seat);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return seats;
	}
	
	@Override
	public List<Seat> findSeatTest() {
		String sql="select * from seattbl";
		
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Seat> seats = new ArrayList<Seat>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();
			
			while (rs.next()) {
				Seat seat = new Seat();
				seat.setSeatId(rs.getInt(1));
				seat.setSeatName(rs.getInt(2));
				seat.setSeatType(rs.getString(3));
				seats.add(seat);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return seats;
	}


	@Override
	public List<Movie> findByMovie(String movieName) {
	String sql="select * from movietbl where moviename = ?";
		
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Movie> movies = new ArrayList<Movie>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, movieName);
			rs = psmt.executeQuery();
			
			while (rs.next()) {
				Movie movie = new Movie();
				movie.setMovieId(rs.getInt(1));
				movie.setName(rs.getString(2));
				movie.setsDate(rs.getString(3));
				movie.seteDate(rs.getString(4));
				movie.setAge(rs.getString(5));
				movie.setGenre(rs.getString(6));
				movie.setDir(rs.getString(7));
				movie.setActor(rs.getString(8));
				movies.add(movie);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return movies;
	
	}

	@Override
	public Movie select(int movieId) {

		String sql = "SELECT movieName, movieGenre, movieDir, movieActor, movieAge FROM movietbl WHERE movieId= ? ";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Movie movie= null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, movieId);
			rs = psmt.executeQuery();

			if (rs.next()) {
				movie = new Movie();
				movie.setName(rs.getString(1));
				movie.setGenre(rs.getString(2));
				movie.setDir(rs.getString(3));
				movie.setActor(rs.getString(4));
				movie.setAge(rs.getString(5));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return movie;
	}
}