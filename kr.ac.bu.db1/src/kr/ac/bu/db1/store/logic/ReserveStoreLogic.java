package kr.ac.bu.db1.store.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.ac.bu.db1.domain.Reserve;
import kr.ac.bu.db1.domain.Reserve2;
import kr.ac.bu.db1.store.ReserveStore;
import kr.ac.bu.db1.store.factory.ConnectionFactory;
import kr.ac.bu.db1.store.logic.util.JdbcUtils;

@Repository
public class ReserveStoreLogic implements ReserveStore {

	private ConnectionFactory connectionFactory;

	public ReserveStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Override
	public void create(Reserve reserve) {
		System.out.println("store" + reserve.getReservePeople());
		Connection connection = null;
		PreparedStatement psmt = null;

		try {
			connection = connectionFactory.createConnection();
			System.out.println("store" + reserve.getReservePeople());
			psmt = connection.prepareStatement(
					"INSERT INTO reservetbl(reserveDate, reservePeople, reservePay, userId) VALUES (?,?,?,?)");

			psmt.setString(1, reserve.getReserveDate());
			psmt.setInt(2, reserve.getReservePeople());
			psmt.setInt(3, reserve.getReservePay());
			psmt.setString(4, reserve.getUserId());
			psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, connection);
		}

	}

	@Override
	public void reserve(Reserve reserve, Reserve2 reserve2) {
		Connection connection = null;
		PreparedStatement psmt = null;

		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement(
					"INSERT INTO reservetbl(reserveId, reserveDate, reservePeople, reservePay, userId, theaterName) VALUES (null,?,?,?,?,?)");
			System.out.println("store " + reserve2.geteDate());
			System.out.println("store " + reserve2.getTheaterName());
			System.out.println("store " + reserve.getReservePeople());
			System.out.println("store " + reserve.getReservePay());
			System.out.println("store " + reserve.getUserId());
			psmt.setString(1, reserve2.geteDate());
			psmt.setInt(2, reserve.getReservePeople());
			psmt.setInt(3, reserve.getReservePay());
			psmt.setString(4, reserve.getUserId());
			psmt.setString(5, reserve2.getTheaterName());
			psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, connection);
		}

	}

}
