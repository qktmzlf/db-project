package kr.ac.bu.db1.store.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import kr.ac.bu.db1.domain.Reserve;
import kr.ac.bu.db1.domain.User;
import kr.ac.bu.db1.store.UserStore;
import kr.ac.bu.db1.store.factory.ConnectionFactory;
import kr.ac.bu.db1.store.logic.util.JdbcUtils;


@Repository
public class UserStoreLogic implements UserStore {

	private ConnectionFactory connectionFactory;
	

	public UserStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Override
	public boolean create(User user, int memberId) {

		Connection connection = null;
		PreparedStatement psmt = null;

		int createdCount = 0;
		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement(
					"INSERT INTO usertbl(userId, userPw, userName, userPhone, userBirth, userAddr, memberId) VALUES (?, ?, ?, ?, ?, ?, ?)");

			psmt.setString(1, user.getUserId());
			psmt.setString(2, user.getUserPw());
			psmt.setString(3, user.getUserName());
			psmt.setString(4, user.getUserPhone());
			psmt.setDate(5, user.getUserBirth());
			psmt.setString(6, user.getUserAddr());
			psmt.setInt(7, memberId);
			

			createdCount = psmt.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, connection);
		}

		return createdCount > 0;
	}

	@Override
	public int create() {

		int memberId = 0;

		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;

		int createdCount = 0;
		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement("INSERT INTO membertbl(memberId, memberName, memberpoint) VALUES (null, 'bronze', 0)");

			createdCount = psmt.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, connection);
		}

		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement("select memberId from membertbl");

			rs = psmt.executeQuery();

			while (rs.next()) {
				memberId = rs.getInt(1);
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, connection);
		}

		return memberId;
	}

	@Override
	public User login(User user) {

		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;

		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement(
					"SELECT userId, userPw, userName, userPhone, userBirth, userAddr, memberId from usertbl where userId = ?");

			psmt.setString(1, user.getUserId());

			rs = psmt.executeQuery();

			if (rs.next()) {
				user = new User();
				user.setUserId(rs.getString(1));
				user.setUserPw(rs.getString(2));
				user.setUserName(rs.getString(3));
				user.setUserPhone(rs.getString(4));
				user.setUserBirth(rs.getDate(5));
				user.setUserAddr(rs.getString(6));
				user.setMemberId(rs.getString(7));

			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, connection, rs);
		}

		return user;
	}

	@Override
	public boolean delete(String userId, String userPw) {

		Connection connection = null;
		PreparedStatement psmt = null;
		int delSuccess = 0;

		System.out.println("store" + userId + userPw);

		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement("delete from usertbl where userId = ? and userPw = ?");

			psmt.setString(1, userId);
			psmt.setString(2, userPw);

			delSuccess = psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			JdbcUtils.close(psmt, connection);
		}
		return delSuccess > 0;
	}

	@Override
	public boolean modify(User user) {
		Connection conn = null;
		PreparedStatement psmt = null;
		int updateCount = 0;
		try {
			conn = connectionFactory.createConnection();

			String sql = "UPDATE usertbl SET userPw = ?, userName = ?, userPhone = ? WHERE userId = ?";
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, user.getUserPw());
			psmt.setString(2, user.getUserName());
			psmt.setString(3, user.getUserPhone());
			psmt.setDate(4, user.getUserBirth());
			psmt.setString(5, user.getUserAddr());
			psmt.setString(6, user.getUserId());
			updateCount = psmt.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
		return updateCount > 0;

	}

	@Override
	public List<Reserve> info(String userId) {
		List<Reserve> reserves = new ArrayList();
		User user = null;
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement("SELECT reserveId from reservetbl where userId = ?");

			psmt.setString(1, userId);

			rs = psmt.executeQuery();
			
			while (rs.next()) {
				Reserve reserve = new Reserve();
				
				reserve.setReserveId(rs.getInt(1));
				
				System.out.println(reserve.getReserveId());
				reserves.add(reserve);
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, connection, rs);
		}
		
		
		List<Reserve> reserves1 = new ArrayList();
		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement("select distinct m.movieName, m.movieStime, m.movieSdate from movietbl m join theatertbl t on t.movieId = m.movieId join reservetbl r on t.theaterName = r.theaterName and r.userId = ?");

			psmt.setString(1, userId);

			rs = psmt.executeQuery();

			while (rs.next()) {
				int i = 0;
				Reserve reserve = null;
				reserve = reserves.get(i);
				System.out.println();
				System.out.println(reserve.getReserveId());
				
				reserve.setMovieName(rs.getString(1));
				reserve.setsTime(rs.getString(2));
				reserve.setsDate(rs.getString(3));
//				reserve.setReserveId(rs.getInt(4));
				
				
				reserves1.add(reserve);
				i++;
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, connection, rs);
		}
		
		return reserves1;
	}
}