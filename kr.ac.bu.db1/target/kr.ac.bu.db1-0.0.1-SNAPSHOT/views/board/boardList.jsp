﻿﻿<!DOCTYPE html>

<html lang="ko">
<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<title>PLAN</title>

<meta charset="utf-8">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<style type="text/css">


header {
	text-align: center;
}

div.container {
	padding-right: 100px;
	padding-left: 100px;
	margin-right: auto;
	margin-left: auto;
	display: block;
}

.container .jumbotron {
	border-radius: 6px;
}

.jumbotron {
	padding-right: 40px margin-bottom: 30px;
	color: white;
	background-image:
		url("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUSExMWFhUVFRcVGBcXFRUXFxYXFRUWFhcWFRUYHSggGBolGxcVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy8mICUvLS0tLzUtLy0tLS0tLS0tLS0vLS8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAADBAACBQEGBwj/xABBEAABAwIEAwUFBQcDBAMBAAABAAIRAyEEEjFBUWFxBRMigZEGMqGxwRRC0eHwI1JTYnKS8RUzohZDVJMkwtIH/8QAGwEAAgMBAQEAAAAAAAAAAAAAAwQBAgUABgf/xAAzEQACAgEDAwICCQQDAQAAAAAAAQIRAwQSIRMxQSJRkfAFFFJhcYGhsdEjYsHhMkLxFf/aAAwDAQACEQMRAD8A5CkK0LoC9ofN7KqQrhq6AuK2UhSESFMqiyNwItVS1MZVUtXWSpC8I+EHiEm0qZVBTOqh8ovGfNnocXUBAsProvPV6cErTw5OQFL4phnRK4lsdGhqpdWKmVrSGNaOpWlRo52gg7JKNls9itg9Pqh5pVGw+khuyU+1UO4Ps0MbpLj8EbFNgS5OsJOlvmVn9svyiDJnb8VlxlKc+TdlGOPHweM7UrS82gCwhIlOYlhLiSIkoORb8KUUjx+Sbc22BhSEw2kToFH0iNQr7kVt9xaF2EQhcIVrO3FYXFeFyFJ1lV7L2a7NBa13G5K8lSpyvoHYpY2m2SAVn/SGRrHSNT6JhGeZuXhG/QphoRSlcOZMynoXm5dz18ewpUZKXqJ9zEvVpqYyKTiZ1VKvCbxKSem4CWQC8IAwxcYTbWJymwNCK8m3sLdLe+TP+xKJw1eSijfMt08Z8/hWCkKwC9EeMbIGruVdARqQPCyo3RC5dAIUhaTaAOoRPsbUN5khlaSb7CFFvQppuHadkT7HGiI2kUOU77DOLA48SQrUwQ4KlOiW6HyK06YO6IaAKH1WuGNLSRbuPDF8PVnwkBWxGEBj9BEGGR8Ky8HQoMpVyhyGNyW2ZmuwhBC0ez/e8lfEYchcwzIN1SU90QmPF058G5RbGuizcc3MST5LSpvkdEpVp2SON1KzSyK40YFXCgldp9mtIWi6hN1enQMJ3rOuGZ31aLfKMWrgiAYssrFZpgjTRejxDCFi9osOqbwTt8mZrsKjG4mYQpCIWrkJ2zJsHCkK0Loapsmw2CeQ4Ab2XsKOG8IP6nivM9m5G+JxuNB9VrHt1sZQCeHD4rP1UZzfpRr/AEfkx4oN5JLnsjYwNXK6S+Fs4PHh1gZ9V4qkx9R28ctl6/sjC5AszVY4xVt8m7os88jpLg0nPSWIqImIqLOrPSeOFj2WdA69SUBjJRW0yU7RoQmHJRQooObsC2jAQahTlUJOsQFEHZM1SFiou3XEwKHjIUAVoXYW9Z4uzkJvBkJYBN0XcR6Ic+wbB/zsfbSkWV2cwq0ag4poDjdIybXc3scVLlEYwKxw6s1qYpoEpNdh2EE+4t3C62mnm013uUN5Qyw+wq2kijDg6phtJWyx0Q3kDRx+4EUrRt8kF+HI5pzuxqD5Loaqb6LvGmApDdRxlGhcDIUbidvgoBIQssbo73HYID2OP5qYlZCleOCy8ZTBF1qYhpG6Qq029U7hdGbqVuVUYxgSA2fiqNoE8AtRzNgEIYTqnllMWWmd+/6GWWqQn62EgTCVLDwRozTEsmOUHTRWkGz4pjl89F6B+Ew8gMIcDAI5jWSNp+Sw6dAlev8AZvswCHOFkpq8ihHdbNP6Lwyyy2bVXu/BqYHBsaLNga+acNUDRWrNtZLVGwsDdvds9ht6apIHXrShU6RciMpSU01sK+7auAai5O2Up0wFyo9dqOSOIqKIx3M6clFErV0hWrLtSUB4TuOCRnZcrZO8PFRV7oqI1IW3yMUYVXbhuS1O4U7lM9czVoUvBnCirtpp7uFw0VHVsItNXgXDeSIyRoVful3u1VyTCxg0EY/imadRKtCKxBkkOY5ND1N6ZZdI03JqmlZofxyDGmVEWi5MtYlpTruNRhfYQ7gzIRG0iNk+2FBVbMIbyt+AixJCTKM7eqsaBWlEoT28kN5WEWJAabICzq9AySZ10Av+C1A/ioSFEMzi7OniUlR544RztBO2oQ34N41Yd9Br0Xp2NGwUfI0TC1j9hZ6KPueTpU+IhWp4claWKbe439VWk6E31m1aFOgk6ZnYrAgNMxOyxn0SvT12FxSVbC35I+HPXcT1WlUnwZeAw5LwBe+69vhKQa0DRZfZWBEh0aInaPbdNhIO3AhK6rJLNPbEd0OGOmxuU33NDvfQJZ+IBKzX9tUnCxjlCdwjMwzDRB6W3mSGOtvdRdjjHKr6q4WoL1VRTLym0ir6iXeUYsVm0EZNIWlukJ9zK73KeFJWFJT1SnRM77OotLIFF3VZ31dGecLwVHYcrT7hTIoWcu9OvYyu5XO5WsaY4Kpw6t1yv1YyjSVe6Wm7DIT8OrLMmUena8CPdLoppk0VXIQrdQp06OMYjsp8EMFFYUOTYeCQzTCapmEnTcjhyVyDuMM66HUpyugq4KW6m3sMbNxyhiNjr800TawSxoE7I7PAPEbKG0+USk13AVKjYk6pWm/MVfGZSJQaRtZdCttkSvdQ+XxEKOclQeas2rtdDthOCYqgHCT9beSzq1Zos0Zj8FoYirDTBE+vqsXu09pba9Rn6tqL9PcI6u4j6BcoUS47xuj4fCkpysHU6ZyCT80aWeMeF3BQwSl6pdjE7axThZshosBpMLzlTsyrVMgGFs03TUIqy2NitbDY2kwZbx5JmOR4l6VyKZcMdQ/W+DzOG7I7v33L0HY+IBJh1oiL3SfatdvviXbm2g4ELHw+IN3U7XkgK7Us0bkBUoaeajD/AGe6bdXFFL9i1S+nJF+kDyWgSsyc9stprwipRUgYpQplVioqby21FVUhEhcK7qI7YCyqIi6p6pGw6pCrK7KS3sc2I7C5lUldlT1Wd00chRdlRR1mT0wD6aHkCaKqQix1XuDlpk+wuaAQxTgpkhUKMtSmCenLtYCNEMWVg9cLwlfrDUmnyhroJpNdwjXIlM3ShKgqpec/MQsY+GaL68JMuL9ShVapKC5Ew78qu6B5Kh4LYusBZqADm0suOYrUYBvK0FFQhx3EnJylz2NHCYe0lAxdczAXGYmOiE/WQlMcv6n9RfwMZF6PQWp0HOuSjtwrUKnWKP3llGfUZIv7vuOw4cbXuw7BGi4+qAQDqUMPtJsst9cvfMwBIlRgXVbL5ZqCRTth8uM2bAuIJ815bFYiDDXEhbuJqgOLbEGI5yqUmUzYtAI4WnzW5gfTiuDC1CeWVJ0ZuCwVV8XIB4zovSdn4FkNaQLWJiJ6qdn42HBrWwzzN+pWr3nRI6zVzTpqhvR6SFWnZemwNECwUJSHaHaLKTC97g1o1JMeQ4nkF5TCf/0Og6oWva5jJhr7ERH3wLi/CdUjjWXKnKEW0h3JPFjajNpWe6lclJUcQHAOa4EHQgyD5hFBQ1kfku4rwHzKpeqBRFTQOTZbOoqKK9opyCZX6q32hYI9pcN/Gb6O/BW/6kw38Zvx/BaD0rf/AF/QVWrj9pG8a4Xe+Cwh7R4b+Mz1Vh7QYb+Mz1Q3o/7WEWtX2kbYrhTv1jjt7Dfxmf3Kw7dwv8en/cEN6L+1hFrF9pGv3qneLKHbuF/j0/7wu/63hv8AyKf97fxQ5aL2TCLWR8yXxRqZlwrK/wBYw3/kUz1e38VcdrUNq9P+9v4ob0M/H7BI63H7r4oecFQpb7fS/is/uaunFsiz2E7eIX81aOmnHuS9TFkxmLZSYalRwaxuribD8Ty3WX2J7TUMUXNpkhzfuvEFzf3mibj4jdfNvbztSsK+TEnNADmtpOaaTWvzAZZgl3hdJN/KAMLsrtMPqs7nvW1JJaQGCMjHPPjzxMA2TcdPicPU+RKepzrJ6I8fuffe8HFZ/bHbdLDMz1Ha+60e888Gj66BBwVSuaPi7o1stocchMWL4Hh5gTpbW3yL2h7WIrv7/vKlVrspIyBtrgM8Vm30jfiox6fDu7lsuozbeFZ9Z9nvaKni2+Hw1GjxUyZI5tP3m89t9lq5l8M7E7TFSu3uBVbUHia79mIItrmvrEbyvteDe/ux3hp95F8rvATsRNwDa145os1C/SwEJZHH1qmC7Y7ZpYan3lQ62a0e888Gj5nQLE9nvbRtV5p12tplx8BBOW+jHE6O56Hlv4L2uxj2Yl4xRc98S0sy5MkkAUw42br+ZusZ3b9I6MqSOTD/APZT0cMovc+X5KPLqFNbY8Lx7n6FlRtW8LznsQcQKUVyMglrczh3jcpjKf3mxoZkRF9t3FvORxpup95Hhzuhsn96LwsmeJytPn2ZqRyVTXHujP8AaT2ibhqcu98+4wEZnHz0bxPzXzqj7YYptY1S8ODvepn/AG4GzW/d1N9eMpP2mD6Nd/2nPUeZdna5ha4C3h0gaiIELzlTt6lp3VTl4m/Fa2iw4MWKnTb78My9XLU5MlxTS8H2TsntzD4pvgaA8CXU3ZczeYj3m8x5xon20CTZnnEfFfF+xsc6o8GjTqBzHN8TagDmlxMEQOTl927HdWazLiX0nPBjMx3vDi4ECHdLdEPValYf+HPxLYMDyP18fAlPBECCfJv4pftfthmFZnqmB90NMveeAB6iToFrio3ZzfUL5n7Z9gYprhWc5mIc8kQS6mGAQQGC4i5tPO5SOPMs06zOl+D+fiOZMTxxvCrf4r5+Bidu9u1cU8OeYF8rAfC0fV2l/losyEjX7SeHOaaGVzTDmmoBzG3PVLVPaIjSiJB/fkeUBb+LLhhHbDsvuMLJptTOW6a5f3o9L2T2zXw7ppPI4tN2u6t0K+hdg+29GrDa0UX8z+zPR33ejvUr4x/1MD/2eH3/AF+70TnZOOfiandtpAe7cvOjqtOlcBvGoD6pfUY9NmVvh+9B8H1vA+Fx7Wv5P0SwyrLzHst7OYnC5f8A5LX0jc0u7dAB0LHFxyn4FepyLDlFRlUXaNmEnKNyVMrlXETKou5JpHygYMSQWX4RfzACIcCIkNBvsDvtwRaz6YHibpFxReZ21Ph9OWqaZjABc76ua1mUEWyZhGsaFetc34PD9JeWLf6YYBLWjkSAT5ob8I0XgADUyIvoNPzTuFxDfuOa7YgPbceTJJ6JulAaYhxvLO9dEk2sIIsdIjqhvJJBVgizOpdlgifCfXyvH6srVOxuEG4H3r/234bbpunWd4S2BBj3xaBFyQ2Db47LsPqNIzE3DSBUFiBva/SYvxVOpK+5fpY6/wDRF/ZWhDZB5EcNbcDwU/0sW05AJl1J4JLidmiSCBrqcx+S4Abloc7bMRT3g+E5uY24K29+5R44+wIYANaTZxE2a5v1/FZRw73VHZu9bJY0Nafdh0k6EH5yAenoA1zYnPEl2jYPoDN4FkdrCdAIHURrqYj5EWQZu+4xiuD9P7CLqTgHw7EGBA3m7/5eGT1CFi6tRrajm1MUDYNGUE6Pj/t3MwZ0W/SbFjMgk/e34GAANLcZVhXsZ14a76kja4SjimzTjmkl3XwPlPbAZVe51ani3uzOAcR9wVKppm4E+At0i5NrpF3Z9BtUOpUcUGh0tLvC73Wz4oiff0B2EL6pi8ECfERN/ugT5efHZPdm4BjYLQJO0QSJ1jy6Ks9kI3bIx58857aR4vsenUo56ja9Sk3Qmo6k+o6TTLblpEy95jWOhC8zjqWEf431Kzqj8rnnJHiI8YFhpAC+4Op2OaB5uItrusXHdnUnXOWToQNb7Cem6UwuLlbfP5DefJlhGopP8W0fHadGjSqh1A1ZDbOIbIOUTYnjpbhwXs/ZipUohop1GNFRzWu72mC4/tzTDmxUEwwgybTA5reHZtNpj5tMbfNO08Azem3QXGXbkb/Db0eeKLXD/YzY/SGbdTj+rPm3tPSpVnvq1cQ11VrWnKG5dajw5kZiQQAXafeA5rKxHZlBhLG4gPaS2CGxMzmk3jpaeK9/2l2DSze60zsQ2fikv9DpT7jb9Jn5yrx0zqt3H4IHL6X2y5g7/GzL7KqVCS4VWOLXNqNpwQXONUQ1pyaXNiPlf3DO2MQ7LGGo5iHkjvnDLkyyHDudfFsksB7PMA8PhjKYBn3DmbfYB0brRfgnsE943NlfY+KRUc0u15t+MIcsCXZ2MY/pGbVyTS+fvPAe1rauJcahyMa2l3nhMgMeKTzYkEkZ2fHWF53E+ylUXdoCQD4dWgOMDMTuNt16ftPsHOYL3ENbkAhos0ACwGsAX+KzX+zYO/SQLDw+ECLCRPmVdYMiVUqO/wDqYpO936CWCwlWg4PpZBBDDmLnB9SmM9gJkmRYG+gC28BjCx4qYhvhYSwtayoaZmmXy5zrF8XH8onmkB7OnNq0xJADSBB2PLdanZuAdTc0up2FQO8J2DMg8PmVaOCaYPJ9I42qtHoKPaeCmDTeDaxoVLTECwOpj9FB9p+2aQpsZRaWVMwPja6kC3K4G74JvFgCmcRjP2ddrG5S5oLAREvAN/UNS3tL2hUeaZY3MWtL81oDqdXD1WgAGTdh2G6nLCUlUkdg1OOLTi1f5Hge0WPc+o85DnAENeD93LMmOqyh2fVuchIHAF07QMs/gvRMxtdlUEy0B4MhjhYMcyxJESHkHfzV39uZX4WpTpU/2YyublyzDXNl9yHG5uYvHBCUtrpRHnqHNctP5s8tR7JqZHv7uoHNy5WGm79oCSHRv4QAbA+SZ7I+1Un524esC4ENy0qtyCHDa4zNFltYbGta2n3lOm8tOWHE5XnOSGmIvlJ8Rn3W9ELs7O0AZQ99OqCWkjK9rg05X+GXNEO0I1voqq2+zI+s0m3Xz/v9x8+1uLLHF1PETkeA5rXAN8UkkiwLQQJ+7IXuPYqo6rTqDEd4agLH+JjpAe2Q0QLQADBuM8xcLxrqmsse2atVzSHlpyPBLGNA1DIBA1NwtD2QwTq+YValVxMAZH14Y4Q4kvY6C0tc3UbIl26/xQGWeFcfyfSB2fS/d/4u/BRYrPZkRrW/95PxN1F3PuD3r7P6GcA3RofFz7gJjq4eHUq9TGta2MrgTa76LSR/TJgJOr2k0g5axMRYNEz1AdfzEWUo4w5QS6q8DYtdbiJJA8yD0Wg1Zjp0HwuNadAXREGc3kXODWjffzRqvadTMSKJyxA90kRuCQQfil3V2ACfC6ZHePBEcxLeY1V24hhHgeydYIeCdPdMlp8z+Kq0r7Fk2lw+AtPtKs7UFsA+9k87wIPQIhxLvvVPEARq2prwDREWCE3M3RuHLdRme699gNdetlxuNf7pdRFjemKpPGwmN9lWl4RO5+ZFqj3OtmqOtq2m4AcvEOukqpfrnNYkaTkAHQECDc6KlWpLYIqSD/JTFtI3J11nqqDHublDLXM6ud/cfDx5qyT8IG5q+X/n5+IxhqxI95w4y5o0M23+qI7GU23dEcXEm3XKZ/JLUcbWJOWoS7Ug5TpwYJ+HmmAazx4miHfeLLzabBsxrsVSS9/3LRk64v4DDe02ZImbkxTMRP8AW4Au/UI9PtHMIDXTwzNLuZPjACV+0UpAexjsogZWOcehzNt+ZRndpyCMvhPuwIcPQ3jgPigOH3DSzLzJDP2gk3puAHFzNx1gbbpygyLQRoTDr3va/Xb1Cz2VW5Q6HQRNySec5pIjT9BHoY3OQ2Mr9BmGWIBk08zZNptA9Epli32Q1hyK+XyalTNluYd1AvykdNRZJYt4IubgSNzfYHSdVdlcQcoYCD7zQNSNmzqZ1k66LPxlRniP7SctsxY2eJygDMPLjyQseLnkYz51t4Z2q4HcAjeWyZ5b8DHkjYNwuJngDAF9s0a68Vl981+kNOnhJjxG7gADedzxRsK1zDDs+ky0NcDeNBeLTIhPVUDJjO8qdWvnuMY8gCDAO9pHSTpubx9VnVKbBEaxJ/Zki8WDgQONk3XJDgA4gR+621zNxfLF/PRZtWs+n7zYbEF0UzPAE55E2mRsFSDl2QbIoN218/kbOAqlozGCDJHvXPUCAAbceq0KdYm5BAv4MoA3v42t+C8zg+1KbfepvyyYLQbcc5JAJPKdUWp2wJzZZbcZYdOY6TLnAcIAvCmWKT8F4aiMV3J7QsYcuVwLid7AcgG21k3PzXnnl3G/X6m618T2lTd91zY/lAAsIHiFrdJ4LNquabjcbXHnGiaxWlTM7UOLk2i1MOBu7n+Cbp2vty16JOmBFp6EJilWj14fERaeqYTEJq2aDqLZEGZG9r/uybSqYiiAPejaLTO8RqqVHgmYBEaC0GdTY31VK46Rrx+IUcsq1FeBVzBO3DUBDdh2769QimgDqP15rhw/6B/JTRKl94nU7MY43Y08LDyuFR3YVOZyZeYLh0T4px/ifomu4tN44kmBb4KNq9i/XyLtJmKOxLtcHuGR2ZoJBghrmyPJxW37OUxQJs2XEuJggFztXPg30/W5KAbuJ5hxH5FOtpgAkGdLAgjzd9VSUI+xeGpzXdjT+0Xzaq0D+ln1EqIUcAY5B8eRgyuoXSiNfXM3zZ5mo+mHRmba9iJnhlaxx/WqI3EBwghxPQAAc8xDjpwSXcCLVJk6Ma58dWgD1KjaDB7z3TxIEzyaBATRR0hr7URwINoDCRwmWx9NEWmSYikWjY28Xm530KQ70n3WudxLiXfAAR5rjK17hs/zAuPkGgj1XNEcjFTDSfEZJ3NVpNuMErowFU6F0HZhEGdLxfTdEpVQACXubGgFNrW+ZdMlFqVKMeMmSN8gd1lomI4gKrkzvzK0sGWjM6m0c3FzydxIa2FKdU6Bszs1uQdQDJPoEChUw4uWj/2PjrGqK7GUyAADG/iaJ43LdPVQRJ+wdtR4FmtG853nzidVfAVzOVxzHYNbUJHKQSB5JP7S1t2t9XNd8UT7eTcmBzLQPQD5KJRspGbjyaYrtaZiN4daL6yAZPIyqiq3ZoEkAnS06zl+qznVWOsSCOGYQPKFWGFv+6QRo0H6eqpsRPWbNI1nyYyDUXtN4F8xk8jZOd1Vgg0GOLIh1hUvuCKlvJ26yG0/DZzjaTIE9Z3RqWJNN0taRtDm1HlsgeKC7S2qFkxuuPn9Qunz8+pv9P8AKHmuq6AA6e+17Bf7pcXl1+E+qVfgBHiJLgbBsAAa2cReEria5e/MWiSNGMAzWnxNdcqriG+F2YTecrmjpMgG/JVSa8kzyRfZWXB1EmZMTB3v92NhyUBbcObMaO8QgiNxpPpwSdZjDAInfV3XVWzNYNRl/mN77AgR5hGaFYsc+0EANbmgXLZBB3nMWfVQPEyWPa4x4sjCSI5X1SlTJILQL7Rpw6+aMK0NzC/IGHN6gxZVUF3CPNPt3LsrU80Et0gnKWHXiQQT6ItOjNg9o5OpUyfUm58kmMWQJBEHWQ4QOoMlUpY0EEB7XDZtQRpswlsne3RRJPwHxztcjWJriAwVWuduG02W1izDlBvzSVare97xOnwhVqYwaSGnQtNNsesC/khmv4gYtvAIB02kx6q0eEUyep2GDhuPSPjqrhw0IkaaSRwi+nJLZfIX10+KK3qDz2+aMmLOI0XBsASQeEn4j6qzhF7m0/r/AAh06sQduOo81aq3cDyBPwBPyVgLryUNTouOq7T8SPkhVGTobqjgY/X1UNl1BDLawkRr1+aZp1/zINuMEcFmNqxcggDf8pT9JxuTptEOB52XJkTx0aLqRIBYGcyJBBtsY+KhZYAtAOskNBPOW6jmkzUyQ4EtkwDIEgag/BP4TE5oDgNLQ0kdbe7/AGwqNtF4JS4fAQNIsaZ+B+JUUPZzTcNoGf5D/wDhcVNwdY/u/Y8W3Gu0blaOAl0eZ/JXYZvnvyafjIj4FHk2DQ1kaBoBJ+fzVaznA+JxcdwHW8z+CYstuT7HGENMzf8Aec53/G8/BF78b1HO5DQckm6sdPAOgk+p8SXdUMxBM8Jn+3fzUWTsch6piP6W/wBIl395khJPiSYzevxOpRqbDuCBwufU6DyRWB5936BQde0FTpvI0yjjEBGZhW83HkJ/KEdzNiJdtJsPIoTj+8fT8lwPe32LNpu/dy9IBKgwpOoPmTHkFelR3v5oxc8dOkrgbm/AFtAaeEdbz8FHWG1uk+oVvtQkBwgb7GOkKQw7kcCb26wqtnc+SVIiI5ToDP8AUNUamAQJBI4h4Bb5T9FRrTGXPLToZAAjhwVMjWkw4ydzcqvJ1oZLHAwx0f1WJHTiqvcePXNb4oGKrxflr+hCrSM3vw6/C6hEU6sbbl1tz4dbfloq90wzAueDnQfKUJkA+78IA6K2YAwQQedwVcpz4I8RGgaZtxngYsVbQXMxuYmOrdr9EHOLyDrpt5Efih1awtBjhOYfG8qjCJN8DPfDQ24T+B/JdfWaP9zKRP7pjpqY6CNUKm5xFodztbeBvzhW7oOEOyga7fUW9VFWTaiyzqlM+6AdSB3j5Gk6GQPJDZrsOQm3WUJuFYPdjlDjr5/imGk2kyTprP5KYpHTl7FC8SQANbwAT5xdSoQ2xjz+io4id+E8PNWMkAGDFtDccDz9Vcijjg6ZaY6z/gqzM4jMQefD8VWmALQI5CIRDbS4+XmpIb8FnsP5T8kMMPl+rFUqVi02aYO0kx6rrHtM2j4fOyizknQTvHA+X63+asGmZB05odGsQS12h0dEidtDb4JmpSAuNY4jfQqUysuGU74tMubPy9Nuqbw2JbLS1xaQDE6DeJ1if8LPLjqZ6jS3EDVEEH7oPlf4aru51Ubf2537tE8zIPmDCiwGj+X0L4XVXYg25+5ht7x5nM509APSU5SoEWc5ojWAf8esrqisMZZerai/hJgMnqfloimoGj3Wgcp+PH0UUXAnFWkWFT72UQNzefr8kIYk/dgA8teqiiiyFFU2EMm0ieCjW5TeATpAufPbyXFFLZSuaOOq68eapTqlt+PAqKKrZdRXYscby6zdEZUBEwZvw2XFFFnSxxS4BsrbC/XWUQtcbOAg7jbfQqKKrZE0ovg7TokC7pj7uUR6nTZdZidojbZRRciqSldkybhxuhOc4QHGcvEeiiilnR5dF+9ProL69UrWlxhrt77eRUUUNhIJK2i1HCuB4dDvPy5o7GG8TzvEKKKexSUmxXHhwgFs21OUnhBO6FTrkWv6riiixjGlKCs0MPVzaoD3Pa+PeZJvMQen+VFFbwBSSk0MZJk3Ea6XHFVnwk6xpOvSQoouKr+ClNx5i+99ZVKz3NuYidv8LiigJFJyS9y9OsAy4IB0IvvrHBHFVzCJFhBBsdQI1PD9BcUUnSgr+JoUSHixjYxI/Xks/EA03gEzsGu47eJtwoouvgBjX9Rx8FHY+LFzp5gO/wCUiVFFFFsdjgg1Z//Z");
	background-attachment: fixed;
}
</style>
</head>
<body>

	







	<!-- Container ======================================================================================= -->
	<div class="container">


	
		<h1>일정 관리</h1>
		&nbsp&nbsp&nbsp&nbsp&nbsp
		<form action="${pageContext.request.contextPath}/board/search.do"
			method="get" class="form-inline my-2my-lg-0">
			<input name="planName" class="form-control mr-sm-2" id="planName"
				type="text" placeholder="게시판 이름을 입력해주세요." value="">
			<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
		</form>
		&nbsp&nbsp&nbsp
		<!-- asdasdasdasdasdasd -->
		<div>
		</div>
		<div>
			<fieldset>
				<c:choose>
					<c:when test="${empty list}">

					</c:when>
					<c:otherwise>
						<c:forEach var="reserve" items="${list}">

							<table class="table">

								<tr>
									<th scope="col">${reserve.sDateTime}</th>
									<th scope="col">${reserve.eDateTime}</th>
									<th scope="col">${reserve.ReservePeople}</th>



								</tr>

							</table>


						</c:forEach>
					</c:otherwise>
				</c:choose>
			</fieldset>




			<!-- Footer ========================================================================================== -->

		</div>
		<!--  <script src="${pageContext.request.contextPath}/resources/js/jquery-2.1.3.js"></script>
<script language="JavaScript">
	function searchBoard(){
		var param = $('#planName').val();
		$.ajax({
			url : "${pageContext.request.contextPath}/board/search.do",
			async: true, 
		    type: 'GET' ,
		    data: {
		    	planName : param
		    }, 
		    dataType: 'text', // xml, json, script, html
		    success: function(result) {
		    	alert('성공!');
		    	console.log(result);
		    	
		    } // 요청 완료 시
		});
	}
</script> -->
</body>
</html>
