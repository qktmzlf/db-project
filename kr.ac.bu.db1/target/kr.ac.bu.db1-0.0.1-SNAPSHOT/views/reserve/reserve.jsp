<!DOCTYPE html>
<html>
<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<title>PLAN</title>

<meta charset="utf-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<meta name="viewport" content="width=device-width, initial-scale=1">


<style type="text/css">

</style>
</head>
<body>


	<div class="container">
		<form action="${pageContext.request.contextPath}/reserve/reserve.do"
			method="post">
			<fieldset>
				<table class="table">

					<tr>
						<th>sDateTime</th>
						<td><input id="sDateTime" name="sDateTime" class="form-control"
							type="datetime-local" value="" placeholder="상영시간"></td>
					</tr>
					<tr>
						<th>eDateTime</th>
						<td><input id="eDateTime" name="eDateTime"
							class="form-control" type="datetime-local" value="" placeholder="상영시간">
						</td>
					</tr>
					<tr>
						<th>reservePeople</th>
						<td><input id="reservePeople" name="reservePeople" class="form-control"
							type="number" value="" placeholder="인원"></td>
					</tr>
				<!-- 	<tr>
						<th>endDate</th>
						<td><input id="eDate" name="eDate" class="form-control"
							type="datetime-local" value="" placeholder="종료날짜"></td>
					</tr> -->
				</table>
				<br>
				<div align="center">
					<input class="btn" type="reset" value="취소"> <input
						class="btn btn-success" type="submit" value="확인">
				</div>
			</fieldset>
		</form>
	</div>

	<br>
</body>
</html>
