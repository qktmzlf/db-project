<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page import="java.security.SecureRandom"%>
<%@ page import="java.math.BigInteger"%>


<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Signin</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/sign-in/">

    <!-- Bootstrap core CSS -->
	


    <style>
    .btn-block {
    display: block;
    width: 100%;
	}
    .form-signin {
    width: 100%;
    max-width: 350px;
    padding: 15px;
    margin: auto;
	}
    
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      
      .text-center {
    text-align: center!important;
}
    </style>
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
  </head>
<body class="text-center">
	<%
		String clientId = "UAfFebYFERnUueeu3YIP";//애플리케이션 클라이언트 아이디값";
		String redirectURI = URLEncoder.encode("YOUR_CALLBACK_URL", "UTF-8");
		SecureRandom random = new SecureRandom();
		String state = new BigInteger(130, random).toString();
		String apiURL = "https://nid.naver.com/oauth2.0/authorize?response_type=code";
		apiURL += "&client_id=" + clientId;
		apiURL += "&redirect_uri=" + redirectURI;
		apiURL += "&state=" + state;
		session.setAttribute("state", state);
	%>
	

		<!-- form -->
		<form action="${pageContext.request.contextPath}/user/login.do"
			method="POST"  class="form-signin">
	<tr>${loginFail }</tr>
			</br></br></br></br>
			<h1 class="h3 mb-3 font-weight-normal">Planner</h1> </br></br>
			<label for="inputEmail" class="sr-only">Email address</label>
			<input type="text" class="form-control" name="userId" placeholder="아이디" required />
			<label for="inputPassword" class="sr-only">Password</label>
		 	<input type="password" class="form-control" name="userPw" placeholder="비밀번호" required />
			</br></br>
			<div class="form-btn">
				<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
			</div>
			</br>
			<div class="form-btn">
				<a href="<%=apiURL%>"><img height="30"
					src="http://static.nid.naver.com/oauth/small_g_in.PNG" /></a>
			</div>

		</form>
		</br></br>
		<p class="text-center">
			계정이 없으신가요? <a
				href="${pageContext.request.contextPath}/views/user/join.jsp"
				class="form-join">회원가입</a>
		</p>


 


</body>
</html>

