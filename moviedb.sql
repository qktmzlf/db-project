drop database movie_db;

create database moviedb;

drop table usertbl;

use moviedb;

create table usertbl
(
	userId varchar(20) not null primary key,
    userPw varchar(20) not null,
    userName varchar(20) not null,
    userPhone varchar(20) not null,
    userBirth date not null,
    userAddr varchar(30) not null,
    memberId int not null,
    foreign key (memberId) references membertbl(memberId)
);

create table nonusertbl
(
	nonPhone varchar(20) not null primary key,
    nonBirth date not null,
    nonPw varchar(20)
);

create table membertbl
(
	memberId int not null auto_increment,
    memberName varchar(10) not null,
    memberPoint int not null,
    primary key(memberId)
);

create table movietbl
(
	movieId int not null auto_increment,
    movieName varchar(20) not null,
    movieStime varchar(20) not null,
    movieSdate varchar(20) not null,
    movieAge varchar(10) not null,
    movieGenre	varchar(10) not null,
    movieDir varchar(20) not null,
    movieActor varchar(40), 
    image varchar(300),
    primary key(movieId)
);

create table seattbl
(
	seatId int not null auto_increment,
    seatName int not null,
    seatType varchar(5) not null,
    primary key(seatId)
);

create table theatertbl
(
	theaterName varchar(10) not null primary key,
    theaterArea varchar(20) not null,
    theaterNum varchar(10) not null,
    movieId	int not null,
    seatId int not null,
    foreign key(movieId) references movietbl(movieId),
    foreign key(seatId) references seattbl(seatId)
);

create table reservetbl
(
	reserveId int not null auto_increment,
    reserveDate date not null,
    reservePeople int not null,
    reservePay int not null,
    userId varchar(20) not null,
    nonPhone varchar(20),
    theaterName varchar(10),
    primary key(reserveId),
    foreign key(userId) references usertbl(userId),
    foreign key(nonPhone) references nonusertbl(nonPhone),
    foreign key(theaterName) references theatertbl(theaterName)
);

Insert into movieTBL Values(null, '기생충', '15:55','2019-05-30', '15', '드라마','봉준호','송강호, 이선균, 조여정, 최우식', '기생충.jpg'); 
Insert into movieTBL Values(null, '기생충', '19:30','2019-05-30', '15', '드라마','봉준호','송강호, 이선균, 조여정, 최우식', '기생충.jpg'); 
Insert into movieTBL Values(null, '기생충', '08:15','2019-05-31', '15', '드라마','봉준호','송강호, 이선균, 조여정, 최우식', '기생충.jpg'); 
Insert into movieTBL Values(null, '기생충', '12:30','2019-05-31', '15', '드라마','봉준호','송강호, 이선균, 조여정, 최우식', '기생충.jpg'); 
Insert into movieTBL Values(null, '기생충', '19:35','2019-06-01', '15', '드라마','봉준호','송강호, 이선균, 조여정, 최우식', '기생충.jpg'); 
Insert into movieTBL Values(null, '고질라 킹 오브 몬스터', '17:40','2019-05:31', '12', '어드벤처, 환타지','마이클 도허티','밀리 바비 브라운, 베라 파미가, 카일 챈들러', '고질라.jpg'); 
Insert into movieTBL Values(null, '고질라 킹 오브 몬스터', '09:00','2019-06-01', '12', '어드벤처, 환타지','마이클 도허티','밀리 바비 브라운, 베라 파미가, 카일 챈들러', '고질라.jpg'); 
Insert into movieTBL Values(null, '고질라 킹 오브 몬스터', '14:20','2019-06-01', '12', '어드벤처, 환타지','마이클 도허티','밀리 바비 브라운, 베라 파미가, 카일 챈들러', '고질라.jpg'); 
Insert into movieTBL Values(null, '고질라 킹 오브 몬스터', '22:45','2019-06-02', '12', '어드벤처, 환타지','마이클 도허티','밀리 바비 브라운, 베라 파미가, 카일 챈들러', '고질라.jpg'); 
Insert into movieTBL Values(null, '고질라 킹 오브 몬스터', '13:30','2019-06-02', '12', '어드벤처, 환타지','마이클 도허티','밀리 바비 브라운, 베라 파미가, 카일 챈들러', '고질라.jpg'); 
Insert into movieTBL Values(null, '알라딘', '20:20','2019-05-30', 'ALL', '가족, 어드벤처, 환타지','가이 리치','메나 마수드, 윌 스미스, 나오미 스콧', '알라딘.jpg'); 
Insert into movieTBL Values(null, '알라딘', '10:00','2019-06-01', 'ALL', '가족, 어드벤처, 환타지','가이 리치','메나 마수드, 윌 스미스, 나오미 스콧', '알라딘.jpg'); 
Insert into movieTBL Values(null, '알라딘', '18:55','2019-06-02', 'ALL', '가족, 어드벤처, 환타지','가이 리치','메나 마수드, 윌 스미스, 나오미 스콧', '알라딘.jpg'); 
Insert into movieTBL Values(null, '악인전', '17:15','2019-05-30', '19', '범죄','이원태','마도억, 김무열, 김성규', '악인전.jpg'); 
Insert into movieTBL Values(null, '악인전', '21:35','2019-05-30', '19', '범죄','이원태','마도억, 김무열, 김성규', '악인전.jpg'); 
Insert into movieTBL Values(null, '악인전', '12-20','2019-06-01', '19', '범죄','이원태','마도억, 김무열, 김성규', '악인전.jpg'); 
Insert into movieTBL Values(null, '악인전', '15:10','2019-06-02', '19', '범죄','이원태','마도억, 김무열, 김성규', '악인전.jpg'); 
Insert into movieTBL Values(null, '어벤져스: 엔드게임', '08-40','2019-06-02', '12', '액션, SF','안소니 루소','로버트 다우니 주니어, 크리스 에반스, 마크버팔로', '엔드게임.jpg'); 
Insert into movieTBL Values(null, '어벤져스: 엔드게임', '12:10','2019-06-02', '12', '액션, SF','안소니 루소','로버트 다우니 주니어, 크리스 에반스, 마크버팔로', '엔드게임.jpg'); 



    
    
    

    
    
    